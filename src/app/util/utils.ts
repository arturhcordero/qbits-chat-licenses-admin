import { Cost } from '../model/product/cost';
import { License } from '../model/account/license/license';
import { Settings } from '../config/settings';

import * as moment from 'moment';
import 'moment/locale/es';

import { PinkToast } from '../components/toastr/pink.toast';
import { Injectable } from '@angular/core';

@Injectable()
export class Utils {


    static calculateLicenseCostStr(cost: Cost): string {

        let monthlyCost = cost.value * 365 / 12;

        return (monthlyCost * (cost.tax.value / 100 + 1)).toFixed(0);
    }

    static calculateLicenseCost(cost: Cost): number {

        let monthlyCost = cost.value * 365 / 12;

        return (monthlyCost * (cost.tax.value / 100 + 1));
    }

    static expirationDescription(license: License) {

        moment.locale(Settings.defaultLocale);

        let expiration = moment(license.expiration);

        let day = expiration.format('D');
        let month = this.capitalize(expiration.format('MMMM'));
        let year = expiration.format('YYYY');

        return `${day} de ${month} de ${year}`;
    }

    static capitalize(s) {
        if (typeof s !== 'string') return '';
        return s.charAt(0).toUpperCase() + s.slice(1);
    }

}
