import {Component, ElementRef, OnInit, Renderer2, TemplateRef, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { AccountInfo, Birthday } from '../../../../model/account/activate/account-info';
import { Settings } from '../../../../config/settings';
import { DateData, Month } from '../../../../model/util/date-data';
import { Moment } from 'moment';
import { cloneDeep } from 'lodash-es';

import { CatalogService } from '../../../../service/catalog.service';
import { GenderReference } from '../../../../model/account/activate/gender-reference';
import { UserService } from '../../../../service/user.service';
import { UserRequest } from '../../../../model/account/activate/user-request';

import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { PinkToast } from '../../../toastr/pink.toast';

import { CountryData } from '../../../../model/util/country-data';

import { countries } from 'country-data';
import * as moment from 'moment';
import 'moment/locale/es';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-activate-user',
    templateUrl: './activate-user.component.html',
    styleUrls: ['./activate-user.component.css']
})
export class ActivateUserComponent implements OnInit {

    @ViewChild('password', {static: true, read: ElementRef}) password: ElementRef;
    @ViewChild('repeatPassword', {static: true, read: ElementRef}) repeatPassword: ElementRef;

    @ViewChild("downloadModal", {static: true}) downloadModal: TemplateRef<any>;

    confirmationToken: string;
    alreadyActivated: boolean = true;
    activated: boolean = false;
    spinnerText: string;
    loading: boolean = false;

    accountInfo: AccountInfo = new AccountInfo();
    dateDate: DateData;
    selectedMonth: Month;

    genderReferences: GenderReference[] = [];
    countryDataList: CountryData[] = [];
    selectedCountry: CountryData;

    spacesToAdd = 2;
    biggestLength = 0;

    constructor(private router: Router,
                private activatedRoute: ActivatedRoute,
                private userService: UserService,
                private catalogService: CatalogService,
                private spinner: NgxSpinnerService,
                private modalService: NgbModal,
                private toastr: ToastrService,
                private renderer: Renderer2) {
    }

    ngOnInit() {

        this.catalogService.findGenderReferences()
            .subscribe(genderReferences => {
                this.genderReferences = genderReferences;
            });

        this.initDateData();
        this.checkFullAge();

        this.initCountries();
        this.processCountryCodes();
        this.countryCodeSelected();

        this.retrieveEmailByToken();
    }

    retrieveEmailByToken() {

        this.activatedRoute.queryParams.subscribe(params => {

            this.confirmationToken = params.confirmationToken;

            this.spinnerText = 'Cargando...';
            this.spinner.show('userActivationSpinner');

            this.loading = true;

            this.userService.emailByConfirmationToken(this.confirmationToken)
                .subscribe(emailToken => {

                    setTimeout(() => {
                        this.alreadyActivated = false;
                        this.accountInfo.email = emailToken.email;
                        this.spinner.hide('userActivationSpinner');
                        this.loading = false;
                    }, 800);

                }, (err) => {

                    setTimeout(() => {
                        this.loading = false;
                        this.alreadyActivated = true;
                        this.spinner.hide('userActivationSpinner');
                    }, 800);

                });
        });

    }

    initCountries() {

        countries.all.filter(c => c.status == 'assigned').forEach(c => {

            let countryData = new CountryData();

            countryData.name = c.name;
            countryData.status = c.status;
            countryData.countryCallingCode = c.countryCallingCodes[0];
            countryData.alpha2 = c.alpha2.toLowerCase();

            if (countryData.countryCallingCode != undefined) {

                this.countryDataList.push(countryData);

                if (countryData.name.toLowerCase().includes('mexico'))
                    this.selectedCountry = countryData;

                let len = countryData.name.length;

                if (len > this.biggestLength)
                    this.biggestLength = len;

            }

        });

    }

    processCountryCodes() {

        let padLength = this.biggestLength + this.spacesToAdd;

        this.countryDataList.forEach(cd => {
            let strLength = cd.name.length;
            for (let i = 0; i < (padLength - strLength); i++)
                cd.name = cd.name + ' ';
            cd.description = cd.name.replace(/ /g, '\u00a0') + cd.countryCallingCode;
        });

    }

    initDateData() {

        moment.locale(Settings.defaultLocale);

        this.dateDate = new DateData();

        for (let i = 0; i < 12; i++) {

            let identifier: string = (i < 10) ? `0${i.toString()}` : i.toString();
            let value: Moment = moment().month(identifier);
            let name: string = value.format("MMMM");

            this.dateDate.addMonth(new Month(identifier, name, value));
        }

        let currentYear: number =  parseInt(moment().format('YYYY'));

        for (let year = currentYear; year >= currentYear - 100; year--) {
            this.dateDate.addYear(year)
        }

        this.selectedMonth = this.dateDate.months[0];

    }

    countryCodeSelectClicked() {
        this.processCountryCodes();
        this.countryCodeSelected();
    }

    countryCodeSelected() {
        this.selectedCountry.description =
            this.selectedCountry.countryCallingCode + '\u00a0\u00a0\u00a0\u00a0\u00a0' + this.selectedCountry.name;
    }

    isBirthDayValid(): boolean {
        return this.accountInfo.birthday.isValid();
    }

    changeMonth(month) {

        this.selectedMonth = this.dateDate.findMonth(month.value);

        let monthLength = this.selectedMonth.dayList.length;
        let dayValue = this.accountInfo.birthday.day;

        if (dayValue !== Settings.emptyValue)
            if (parseInt(dayValue) > monthLength) {
                this.accountInfo.birthday.day = monthLength.toString();
            }

        this.checkFullAge()

    }

    showPassword(element, icon) {

        let nativeElement = undefined;

        switch (element.name) {
            case 'password':
                nativeElement = this.password.nativeElement; break;
            case 'repeatPassword':
                nativeElement = this.repeatPassword.nativeElement; break;
            default:
                nativeElement = this.repeatPassword.nativeElement; break;
        }

        let type = nativeElement.getAttribute('type');

        if (type === 'text') {

            this.renderer.setAttribute(nativeElement, 'type', 'password');

            this.renderer.addClass(icon, 'fa-eye-slash');
            this.renderer.removeClass(icon, 'fa-eye');

        } else {

            this.renderer.setAttribute(nativeElement, 'type', 'text');

            this.renderer.removeClass(icon, 'fa-eye-slash');
            this.renderer.addClass(icon, 'fa-eye');
        }

    }

    checkFullAge() {

        let bithday: Birthday = this.accountInfo.birthday;

        let monthSt = `0${(parseInt(bithday.month) + 1).toString()}`.slice(-2);
        let dateSt = `${bithday.day}-${monthSt}-${bithday.year}`;

        let currentBirthday = moment(dateSt, Settings.defaultDateTimeFormat);
        let yearsDifference: number = moment().diff(currentBirthday, 'years');

        this.accountInfo.birthday.fullAge = yearsDifference >= Settings.defaultFullAge;

    }

    submit() {

        let accountInfo = JSON.parse(JSON.stringify(this.accountInfo)) as AccountInfo;

        accountInfo.birthdaySt = this.accountInfo.birthday.export();
        accountInfo.cellphone.phoneRegion = this.selectedCountry.alpha2.toUpperCase();

        let cellphone = this.accountInfo.cellphone.phone;

        if (cellphone == undefined || cellphone.match(/^\d{10}$/) == null)
            accountInfo.cellphone = undefined;

        let userRequest = new UserRequest(
            accountInfo.email, accountInfo.fullName,
            accountInfo.password,
            accountInfo.birthdaySt,
            accountInfo.genderReferenceId,
            accountInfo.cellphone
        );

        this.spinnerText = 'Activando usuario...';
        this.spinner.show('userActivationSpinner');

        this.userService.confirmUser(this.confirmationToken, userRequest)
            .subscribe(message => {
                this.activated = true;

                this.modalService.open(this.downloadModal, {
                    ariaLabelledBy: 'modal-basic-title',
                    windowClass: 'dark-modal',
                    backdrop: 'static',
                    centered: true
                }).result.then((result) => {

                }, (reason) => {

                });

                this.spinner.hide('userActivationSpinner');
                this.toast('Usuario activado');
            });

    }

    toast(message: string) {

        const opt = cloneDeep(this.toastr.toastrConfig);
        opt.toastComponent = PinkToast;
        opt.toastClass = 'pinktoast';
        opt.positionClass = 'toast-top-right-custom';
        opt.timeOut = 1800;

        this.toastr.show(message, 'Correcto!', opt);
    }

    cancel() {
        this.router.navigate(['/account']);
    }

}
