import { Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';
import { ChangePasswordRequest } from '../../model/password/change-password-request';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { ServerException } from '../../model/util/server-exception';
import { NgxSpinnerService } from 'ngx-spinner';
import { UserService } from '../../service/user.service';
import { DataStorageService } from '../../data/data-storage.service';
import { ToastrService } from 'ngx-toastr';
import { PinkToast } from '../toastr/pink.toast';
import { cloneDeep } from 'lodash-es';

@Component({
    selector: 'app-change-password',
    templateUrl: './change-password.component.html',
    styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {

    changePassword: ChangePasswordRequest;

    exception: ServerException = new ServerException();

    submitted: boolean = false;
    loading: boolean = false;

    @ViewChild('currentPassword', {static: true, read: ElementRef}) currentPassword: ElementRef;
    @ViewChild('newPassword', {static: true, read: ElementRef}) password: ElementRef;
    @ViewChild('repeatNewPassword', {static: true, read: ElementRef}) repeatPassword: ElementRef;

    constructor(private router: Router,
                private spinner: NgxSpinnerService,
                private toastr: ToastrService,
                private dataStorageService: DataStorageService,
                private userService: UserService,
                private renderer: Renderer2) {

    }

    ngOnInit() {
        this.changePassword = new ChangePasswordRequest();
    }

    showPassword(element, icon) {

        let nativeElement = undefined;

        switch (element.name) {
            case 'currentPassword':
                nativeElement = this.currentPassword.nativeElement; break;
            case 'password':
                nativeElement = this.password.nativeElement; break;
            case 'repeatPassword':
                nativeElement = this.repeatPassword.nativeElement; break;
            default:
                nativeElement = this.repeatPassword.nativeElement; break;
        }

        let type = nativeElement.getAttribute('type');

        if (type === 'text') {

            this.renderer.setAttribute(nativeElement, 'type', 'password');

            this.renderer.addClass(icon, 'fa-eye-slash');
            this.renderer.removeClass(icon, 'fa-eye');

        } else {

            this.renderer.setAttribute(nativeElement, 'type', 'text');

            this.renderer.removeClass(icon, 'fa-eye-slash');
            this.renderer.addClass(icon, 'fa-eye');
        }

    }

    submit() {

        const endpoint = `${environment.chatLicensesUrl}/user/changePassword`;

        this.changePassword.email = this.dataStorageService.authenticatedUser.email;

        this.spinner.show('changePasswordConfirmSpinner');

        setTimeout(() => {

            try {

                this.userService.changePassword(this.changePassword)
                    .subscribe((data) => {

                        this.submitted = true;
                        this.loading = false;
                        this.exception = undefined;

                        this.spinner.hide('changePasswordConfirmSpinner');

                        this.toast('Contraseña cambiada correctamente');

                        this.router.navigateByUrl('/account');

                    }, error  => {

                        this.spinner.hide('changePasswordConfirmSpinner');

                        this.exception = <ServerException> error.error;
                        this.loading = false;
                        this.submitted = true;

                    });

            } catch (error) {

                let serverException = new ServerException();
                serverException.message = error;

                this.exception = serverException;
                this.loading = false;
            }

        }, 400);

    }

    cancel() {
        this.router.navigate(['/account']);
    }

    toast(message: string) {

        const opt = cloneDeep(this.toastr.toastrConfig);
        opt.toastComponent = PinkToast;
        opt.toastClass = 'pinktoast';
        opt.positionClass = 'toast-top-right-custom';
        opt.timeOut = 1800;

        this.toastr.show(message, 'Correcto!', opt);
    }

}
