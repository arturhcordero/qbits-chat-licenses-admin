import {AfterViewInit, Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs';
import {UserService} from '../../service/user.service';
import {Organization} from '../../model/organization/organization';
import {DataStorageService} from '../../data/data-storage.service';
import {UserLicenseInfo} from '../../model/account/license/user-license-info';
import {LicenseService} from '../../service/license.service';
import {License} from '../../model/account/license/license';
import {Router} from '@angular/router';
import {LicenseProduct} from '../../model/product/license-product';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Page} from '../../model/util/page';
import {Utils} from '../../util/utils';
import {Order} from '../../model/product/request/order';
import {OrderDetail} from '../../model/product/request/order-detail';
import {PinkToast} from '../toastr/pink.toast';
import {ToastrService} from 'ngx-toastr';
import {LicenseOrderRequest} from '../../model/product/request/license-order-request';
import {ProductOrderService} from '../../service/product-order.service';
import {NgxSpinnerService} from 'ngx-spinner';
import { cloneDeep } from 'lodash-es';
import {LicenseAssignmentRequest} from '../../model/account/license/actions/license-assignment-request';
import {Role} from '../../model/account/role/role';
import {RoleInfo} from '../../model/account/role/role-info';
import PageMethodeModelInfoReturn = DataTables.PageMethodeModelInfoReturn;
import {LicenseUnassignRequest} from '../../model/account/license/actions/license-unassign-request';
import {LicenseCancellationRequest} from '../../model/account/license/actions/license-cancellation-request';

@Component({
    selector: 'app-user-licenses-manager',
    templateUrl: './user-licenses-manager.component.html',
    styleUrls: ['./user-licenses-manager.component.css']
})
export class UserLicensesManagerComponent implements AfterViewInit, OnInit, OnDestroy {

    @ViewChild("shoppingCartModal", {static: true}) shoppingCartModal: TemplateRef<any>;
    @ViewChild("licenseAssignmentModal", {static: true}) licenseAssignmentModal: TemplateRef<any>;
    @ViewChild("licenseUnassignModal", {static: true}) licenseUnassignModal: TemplateRef<any>;
    @ViewChild("userDetailsModal", {static: true}) userDetailsModal: TemplateRef<any>;
    @ViewChild("licenseCancellationModal", {static: true}) licenseCancellationModal: TemplateRef<any>;

    @ViewChild(DataTableDirective, {static: false})
    private usersDatatableElement: DataTableDirective;
    usersTableTrigger: Subject<any> = new Subject();
    userOpts: DataTables.Settings = {};
    dtInstance: DataTables.Api;

    spinnerText: string;

    noLicenses: boolean = false;
    userLicenseList: UserLicenseInfo[];
    licensesList: License[];
    licensesCount: number = 0;

    organization: Organization;

    roles: RoleInfo[];
    userRole: RoleInfo;

    order: Order = new Order();
    currentLicense: License;
    currentUserLicense: UserLicenseInfo;
    licenseAssignmentRequest: LicenseAssignmentRequest;
    licenseCancellationRequest: LicenseCancellationRequest;

    userLicensesPageInfo: PageMethodeModelInfoReturn;
    userLicensesCallback: any;

    constructor(private router: Router,
                private modalService: NgbModal,
                private toastr: ToastrService,
                private spinner: NgxSpinnerService,
                private productOrderService: ProductOrderService,
                private userService: UserService,
                private licenseService: LicenseService,
                private dataStorageService: DataStorageService) {

    }

    ngOnInit() {

        let roles = this.dataStorageService.roles;

        this.roles = roles.filter(role => {
            return  role.code == Role.User ||
                    role.code == Role.Admin
        });

        this.userRole = this.roles
            .filter(role => role.code == Role.User)[0];

        this.licenseAssignmentRequest = new LicenseAssignmentRequest();
        this.licenseCancellationRequest = new LicenseCancellationRequest();
        this.defaultLicenseAssignment();

        this.organization = this.dataStorageService.organization;
        let licenseProduct = this.dataStorageService.currentLicenseProduct;

        let storedOrder = this.dataStorageService.orderLicense;
        if (storedOrder != null)
            this.order = this.retrieveOrder(storedOrder);

        if (licenseProduct != null) {

            this.addOrderDetail(licenseProduct);

            this.dataStorageService.currentLicenseProduct = null;

            this.shoppingCart(false);
        }

        this.loadLicensesTable().subscribe(licenses =>
            this.processLicensesList(licenses));

        this.userLicensesTable();
    }

    changeLicenseOrderCount(orderDetail: OrderDetail, increment: boolean) {

        let orderDetails = this.order.orderDetails;

        if (increment)
            orderDetail.incrementQuantity();
        else {
            if (orderDetail.quantity == 1) {
                let index = orderDetails.indexOf(orderDetail);
                if (index > -1) {
                    orderDetails.splice(index, 1);
                    if (orderDetails.length == 0)
                        this.toastMessage('Carrito vaciado', 2500);
                }
            } else {
                orderDetail.decrementQuantity();
            }
        }
        this.dataStorageService.orderLicense = this.order;
        this.order.calculateTotal();
    }

    changeLicenseCancellationQuantity(increment: boolean) {

        if (increment)
            this.licenseCancellationRequest.incrementQuantity();
        else {
            if (this.licenseCancellationRequest.quantity !== 1) {
                this.licenseCancellationRequest.decrementQuantity();
            }
        }

    }

    defaultOptions() {

        return {
            responsive: true,
            language: {
                lengthMenu: 'Mostrar _MENU_ registros',
                search: 'Buscar: ',
                info: 'Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros',
                searchPlaceholder: 'Email / Nombre',
                paginate: {
                    first: "Primero",
                    last: "Último",
                    next: "Siguiente",
                    previous: "Anterior"
                }
            },
            pageLength: 3,
            lengthMenu: [5, 10, 15],
            searchDelay: 350,
            processing: false, serverSide: true, info: false, lengthChange: false, searching: true,
            columnDefs: [
                { targets: '_all', orderable: false, searchable: false }
            ]
        };

    }

    loadLicensesTable() {
        return this.licenseService.findGroupedLicenses(this.organization.id);
    }

    processLicensesList(licenses) {
        this.licensesList = licenses;
        this.licensesCount = 0;
        this.licensesList.forEach(l => {
            this.licensesCount += l.quantity;
        });
    }

    userLicensesTable() {

        this.userOpts = this.defaultOptions();
        this.userOpts.drawCallback = (settings) => {};

        this.userOpts.ajax = (dataTablesParameters: any, callback, settings) => {
            this.usersDatatableElement.dtInstance.then((dtInstance: DataTables.Api) => {

                let pageInfo = dtInstance.page.info();
                let search = dtInstance.search();

                if (search.length == 1)
                    return;

                this.userLicensesPageInfo = pageInfo;
                this.userLicensesCallback = callback;
                this.dtInstance = dtInstance;

                this.spinnerMessage('Cargando información');

                this.loadUserLicensesTable(search, pageInfo, callback);

            });
        };
        this.userOpts.columns = [
            {data: 'user.email'},
            {data: 'id'}
        ];

    }

    loadUserLicensesTable(search, pageInfo, callback) {
        this.userService.findUsers(search, this.organization.id, pageInfo.page, pageInfo.length)
            .subscribe(ulp => {
                this.loadUserLicensesPage(ulp, callback);
            });
    }

    loadUserLicensesPage(userLicensesPage: Page<UserLicenseInfo>, callback) {

        this.userLicenseList = userLicensesPage.content;

        if (this.userLicenseList.length > 0) {
            callback({
                recordsTotal: userLicensesPage.totalElements,
                recordsFiltered: userLicensesPage.totalElements,
                data: []
            });
        }

        this.spinner.hide('userManagerSpinner');
    }

    showUserDetails(userLicense: UserLicenseInfo) {

        this.currentUserLicense = userLicense;

        this.modalService.open(this.userDetailsModal, {
            ariaLabelledBy: 'modal-basic-title-1',
            centered: true
        }).result.then((result) => {},
            (reason) => {});

    }

    showLicenseUnassign(userLicense: UserLicenseInfo) {

        this.currentUserLicense = userLicense;

        this.modalService.open(this.licenseUnassignModal, {
            ariaLabelledBy: 'modal-basic-title-1',
            centered: true,
            //backdrop : 'static'
        }).result.then((result) => {

        }, (reason) => {

        });

    }

    unassignLicense() {

        let unassignLicenseRequest = new LicenseUnassignRequest();
        unassignLicenseRequest.licenseId = this.currentUserLicense.license.id;

        this.spinnerMessage('Desasignando licencia');

        this.licenseService.unassignLicense(unassignLicenseRequest)
            .subscribe(message => {

                let search = this.dtInstance.search();

                this.loadUserLicensesTable(search, this.userLicensesPageInfo, this.userLicensesCallback);

                this.loadLicensesTable().subscribe(licenses => {
                    this.toastMessage('Licencia desasignada correctamente', 2500);
                    this.processLicensesList(licenses);
                    this.clearSelections();
                    this.modalService.dismissAll();
                });

            }, (err) => {
                this.spinner.hide('userManagerSpinner');
            });

    }

    showUserLicenseAssignment(userLicense: UserLicenseInfo) {
        this.currentUserLicense = userLicense;
        this.licenseAssignmentRequest.email = userLicense.user.email;
        this.showLicenseAssignment(userLicense.license);
    }

    changeCurrentLicense(licenseId: string) {
        this.currentLicense = this.licensesList
            .filter(license => {
                return license.id == parseInt(licenseId)
            })[0];
        this.licenseAssignmentRequest.licenseId = parseInt(licenseId);
    }

    showLicenseCancellation(license: License) {

        this.currentLicense = license;

        this.licenseCancellationRequest.licenseId = this.currentLicense.id;

        this.modalService.open(this.licenseCancellationModal, {
            ariaLabelledBy: 'modal-basic-title-1',
            centered: true
        }).result.then((result) => {
            this.clearSelections();
        }, (reason) => {
            this.licenseCancellationRequest.licenseId = null;
            this.licenseCancellationRequest.quantity = 1;
            this.clearSelections();
        });

    }

    cancelLicenses() {

        this.spinnerMessage('Cancelando licencia(s)');

        this.licenseService.cancelLicenses(this.licenseCancellationRequest)
            .subscribe(message => {

                let search = this.dtInstance.search();

                this.loadUserLicensesTable(search, this.userLicensesPageInfo, this.userLicensesCallback);

                this.loadLicensesTable().subscribe(licenses => {

                    this.processLicensesList(licenses);
                    this.clearSelections();

                    if (this.licenseCancellationRequest.quantity > 1)
                        this.toastMessage('Licencia cancelada correctamente', 2500);
                    else
                        this.toastMessage('Licencias canceladas correctamente', 2500);

                    this.spinner.hide('userManagerSpinner');
                    this.modalService.dismissAll();
                });

            }, (err) => {
                this.spinner.hide('userManagerSpinner');
            });

    }

    showLicenseAssignment(license: License) {

        this.currentLicense = license;

        this.noLicenses = this.licensesList.length == 0;

        if (this.currentLicense != null)
            this.licenseAssignmentRequest.licenseId = this.currentLicense.id;

        this.modalService.open(this.licenseAssignmentModal, {
            ariaLabelledBy: 'modal-basic-title-1',
            centered: true
        }).result.then((result) => {
            this.clearSelections();
            this.defaultLicenseAssignment();
        }, (reason) => {
            this.licenseAssignmentRequest.licenseId = null;
            this.defaultLicenseAssignment();
            this.clearSelections();
        });

    }

    assignLicense() {

        this.spinnerMessage('Asignando licencia');

        this.licenseService.assignLicense(this.licenseAssignmentRequest)
            .subscribe(message => {

                let search = this.dtInstance.search();

                this.loadUserLicensesTable(search, this.userLicensesPageInfo, this.userLicensesCallback);

                this.loadLicensesTable().subscribe(licenses => {

                    this.processLicensesList(licenses);

                    this.clearSelections();
                    this.toastMessage('Licencia asignada correctamente', 2500);
                    this.spinner.hide('userManagerSpinner');
                    this.modalService.dismissAll();
                });

            }, (err) => {
                this.spinner.hide('userManagerSpinner');
            });

    }

    reload() {

        this.spinnerMessage('Actualizando información');

        let search = this.dtInstance.search();
        this.loadUserLicensesTable(search, this.userLicensesPageInfo, this.userLicensesCallback);
    }

    clearSelections() {
        this.currentLicense = null;
        this.currentUserLicense = null;
    }

    defaultLicenseAssignment() {
        this.licenseAssignmentRequest.email = null;
        this.licenseAssignmentRequest.roleId = this.userRole.id;
    }

    cancelLicenseAssignment() {
        this.defaultLicenseAssignment();
        this.modalService.dismissAll();
        this.clearSelections();
    }

    cancelLicenseCancellation() {
        this.modalService.dismissAll();
        this.licenseCancellationRequest.licenseId = null;
        this.licenseCancellationRequest.quantity = 1;
        this.clearSelections();
    }

    shoppingCart(close: boolean) {

        this.order.orderDetails.sort((od1, od2) => {
            return od1.productId > od2.productId ? 1 : -1;
        });

        if (close) {
            this.modalService.dismissAll();
            return;
        }

        this.modalService.open(this.shoppingCartModal, {
            ariaLabelledBy: 'modal-basic-title',
            centered: true
        }).result.then((result) => {

        }, (reason) => {

        });

    }

    addOrderDetail(licenseProduct: LicenseProduct) {

        let cost = licenseProduct.cost;

        licenseProduct.calculatedCostStr = Utils.calculateLicenseCostStr(cost);
        licenseProduct.calculatedCost = Utils.calculateLicenseCost(cost);

        let orderDetailProduct = this.order.orderDetails
            .filter(od => od.productId == licenseProduct.id);

        if (orderDetailProduct.length == 0) {
            this.order.orderDetails.push(new OrderDetail(licenseProduct, 1));
            this.dataStorageService.orderLicense = this.order;
        } else {
            orderDetailProduct[0].incrementQuantity();
        }
        this.dataStorageService.orderLicense = this.order;
        this.order.calculateTotal();
    }

    retrieveOrder(currentOrder: Order): Order {

        let order = new Order();

        currentOrder.orderDetails.forEach(od => {
            order.orderDetails.push(new OrderDetail(od.licenseProduct, od.quantity));
        });

        order.calculateTotal();

        return order;
    }

    submitOrder() {

        let order = new Order();
        order.total = undefined;

        this.order.orderDetails.forEach(od => {
            let orderDetail = new OrderDetail(od.licenseProduct, od.quantity);
            orderDetail.licenseProduct = undefined;
            orderDetail.subtotal = undefined;
            order.orderDetails.push(orderDetail);
        });

        let licenseOrder = new LicenseOrderRequest();
        licenseOrder.organizationId = this.organization.id;
        licenseOrder.order = order;

        this.spinnerMessage('Procesando orden');

        this.productOrderService.createLicenseOrder(licenseOrder)
            .subscribe(message => {
                this.loadLicensesTable().subscribe(licenses => {

                    this.processLicensesList(licenses);

                    this.clearOrder(false);

                    this.modalService.dismissAll();
                    this.toastMessage('Orden procesada correctamente', 2500);
                    this.spinner.hide('userManagerSpinner');
                });

            });

    }

    clearOrder(showMessage: boolean) {
        this.order = new Order();
        this.dataStorageService.orderLicense = this.order;
        if (showMessage)
            this.toastMessage('Carrito vaciado', 2500);
    }

    showLicenseProducts() {
        this.modalService.dismissAll();
        this.router.navigate(['/license-product-visualizer']);
    }

    toastMessage(message: string, time: number) {

        const opt = cloneDeep(this.toastr.toastrConfig);
        opt.toastComponent = PinkToast;
        opt.toastClass = 'pinktoast';
        opt.positionClass = 'toast-top-right-custom';
        opt.timeOut = time;

        this.toastr.show(message, 'Correcto!', opt);
    }

    spinnerMessage(message: string) {
        this.spinnerText = message;
        this.spinner.show('userManagerSpinner');
    }

    ngAfterViewInit(): void {
        this.usersTableTrigger.next();
    }

    ngOnDestroy(): void {
        this.usersTableTrigger.unsubscribe();
    }

}
