import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';

import {ServerException} from '../../../../model/util/server-exception';
import {PaymentMethodRequest} from '../../../../model/payment-method/payment-method-request';
import {PaymentMethodService} from '../../../../service/payment-method.service';
import {DataStorageService} from '../../../../data/data-storage.service';
import {AuthService} from '../../../auth/service/auth.service';
import {NgxSpinnerService} from 'ngx-spinner';

@Component ({
    selector: 'app-add-payment-method-form',
    templateUrl: './add-payment-method-form.component.html',
    styleUrls: ['./add-payment-method-form.component.css']
})
export class AddPaymentMethodFormComponent implements OnInit {

    @ViewChild('cardNumber', {static: true, read: ElementRef}) cardNumber: ElementRef;

    paymentMethod: PaymentMethodRequest;
    organizationId: number;

    exception: ServerException = new ServerException();

    submitted: boolean = false;
    loading: boolean = false;
    cardBrand: string = 'unknown';

    constructor(private router: Router,
                private spinner: NgxSpinnerService,
                public authService: AuthService,
                private paymentMethodService: PaymentMethodService,
                public dataStorageService: DataStorageService) {

        if (!this.authService.isAuthenticated())
            this.router.navigate(['/login']);

        this.organizationId = dataStorageService.license.organizationId;
    }

    ngOnInit() {
        this.paymentMethod = new PaymentMethodRequest();
    }

    switchDefaultMethod() {
        this.paymentMethod.defaultMethod = !this.paymentMethod.defaultMethod;
    }

    checkCardBrand() {

        let nativeElement = this.cardNumber.nativeElement;
        let classList = nativeElement.classList;

        if (nativeElement.value === '') {
            this.cardBrand = 'unknown';
        } else if (classList.contains('visa')) {
            this.cardBrand = 'visa';
        } else if (classList.contains('mastercard')) {
            this.cardBrand = 'mastercard';
        } else {
            this.cardBrand = 'unknown';
        }

    }

    submit() {

        this.loading = true;

        this.spinner.show('editPaymentMethodSpinner');

        setTimeout(() => {

            try {

                let paymentMethodCreate = JSON.parse(JSON.stringify(this.paymentMethod));

                this.paymentMethodService.createPaymentMethod(this.organizationId, paymentMethodCreate)
                    .subscribe(() => {

                        this.exception = undefined;

                        this.paymentMethodService
                            .findPaymentMethods(this.organizationId)
                            .subscribe(paymentMethods => {

                                this.submitted = true;
                                this.loading = false;

                                this.dataStorageService.paymentMethods = paymentMethods;
                                this.spinner.hide('editPaymentMethodSpinner');
                                this.router.navigateByUrl('/payment-method');
                            });

                    }, error  => {

                        this.exception = <ServerException> error.error;
                        this.loading = false;
                        this.submitted = true;

                    });

            } catch (error) {

                let serverException = new ServerException();
                serverException.message = error;

                this.exception = serverException;
                this.loading = false;
            }

        }, 650);

    }

}
