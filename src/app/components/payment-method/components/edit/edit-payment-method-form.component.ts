import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ServerException } from '../../../../model/util/server-exception';
import { PaymentMethod } from '../../../../model/payment-method/payment-method';
import { License } from '../../../../model/account/license/license';

import { PaymentMethodUpdateRequest } from '../../../../model/payment-method/payment-method-update-request';

import { DataStorageService } from '../../../../data/data-storage.service';
import { AuthService } from '../../../auth/service/auth.service';
import { PaymentMethodService } from '../../../../service/payment-method.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component ({
    selector: 'app-edit-payment-method',
    templateUrl: './edit-payment-method-form.component.html',
    styleUrls: ['./edit-payment-method-form.component.css']
})

export class EditPaymentMethodFormComponent implements OnInit {

    organizationId: number;
    paymentMethodInfo: PaymentMethod;
    paymentMethodUpdate: PaymentMethodUpdateRequest = new PaymentMethodUpdateRequest();

    license: License = new License();

    exception: ServerException = new ServerException();

    submitted: boolean = false;
    loading: boolean = false;

    constructor(private router: Router,
                public authService: AuthService,
                private spinner: NgxSpinnerService,
                public paymentMethodService: PaymentMethodService,
                public dataStorageService: DataStorageService) {

        if (!this.authService.isAuthenticated())
            this.router.navigate(['/login']);

        this.organizationId = this.dataStorageService.license.organizationId;
    }

    ngOnInit() {

        this.paymentMethodInfo = this.dataStorageService.paymentMethodUpdate;

        let year = this.paymentMethodInfo.expirationDate.substring(0, 2);
        let month = this.paymentMethodInfo.expirationDate.substring(2, 4);

        this.paymentMethodUpdate.expirationDate = `${month} / ${year}`;
    }

    submit() {

        this.loading = true;

        this.spinner.show('editPaymentMethodSpinner');

        setTimeout(() => {

            try {

                let paymentMethodUpdate = JSON.parse(JSON.stringify(this.paymentMethodUpdate));

                paymentMethodUpdate.expirationDate = this.paymentMethodUpdate
                    .expirationDate.replace(/\s/g, "");

                this.paymentMethodService.updatePaymentMethod(
                    this.paymentMethodInfo.uuid, paymentMethodUpdate
                ).subscribe(() => {

                    this.exception = undefined;

                    this.paymentMethodService
                        .findPaymentMethods(this.organizationId)
                        .subscribe(paymentMethods => {

                            this.submitted = true;
                            this.loading = false;

                            this.dataStorageService.paymentMethods = paymentMethods;
                            this.spinner.hide('editPaymentMethodSpinner');
                            this.router.navigateByUrl('/payment-method');
                        });

                }, error  => {

                    this.exception = <ServerException> error.error;
                    this.loading = false;
                    this.submitted = true;

                });

            } catch (error) {

                let serverException = new ServerException();
                serverException.message = error;

                this.exception = serverException;
                this.loading = false;
            }

        }, 650);

    }
}
