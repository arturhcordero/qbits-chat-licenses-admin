import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-change-email-account',
    templateUrl: './cancel-membership.component.html',
    styleUrls: ['./cancel-membership.component.css']
})
export class CancelMembershipComponent implements OnInit {

    constructor(private router: Router) {

    }

    ngOnInit() {

    }

    submit() {

    }

    cancel() {
        this.router.navigate(['/account']);
    }

}
