import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'

import { AuthIndexComponent } from './container/auth-index/auth-index.component'
import { AuthLoginComponent } from './components/auth-login/auth-login.component'
import { AuthService } from './service/auth.service';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import {HeaderModule} from '../../layout/components/header';

const routes: Routes = [
    {
        path: '',
        component: AuthIndexComponent,
        children: [
            { path: 'login', component: AuthLoginComponent },
            { path: '', redirectTo: 'login', pathMatch: 'full' }
        ],
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        ReactiveFormsModule,
        CommonModule,
        HeaderModule
    ],
    declarations: [
        AuthIndexComponent,
        AuthLoginComponent
    ],
    providers: [
        AuthService
    ]
})
export class AuthModule {}
