import { Component } from '@angular/core'

@Component({
    selector: 'ui-pages-auth',
    template: `
        <app-header></app-header>
        <div class="page-single">
            <div class="container">
                <div class="row">
                    <div class="col col-login mx-auto">
                        <router-outlet></router-outlet>
                    </div>
                </div>
            </div>
        </div>
    `,
    styles: [],
})
export class AuthIndexComponent {  }
