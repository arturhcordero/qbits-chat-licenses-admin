import { Component } from '@angular/core';

@Component({
    selector: 'ui-layout',
    template: `
        <div class="page">
            <div class="page-main">
                <app-header></app-header>
                <router-outlet></router-outlet>
            </div>
            <app-footer></app-footer>
        </div>
    `,
})
export class LayoutComponent {}
