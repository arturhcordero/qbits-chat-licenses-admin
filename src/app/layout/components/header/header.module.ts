import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { RouterModule } from '@angular/router'

import { HeaderComponent } from './components/header/header.component'
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        NgxSpinnerModule,
    ],
    declarations: [
        HeaderComponent
    ],
    exports: [HeaderComponent]
})
export class HeaderModule {}
