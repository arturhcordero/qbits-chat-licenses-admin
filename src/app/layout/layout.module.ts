import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { LayoutBaseComponent } from './components/layout-base/layout-base.component';
import { LayoutComponent } from './components/layout/layout.component';

import { HeaderModule } from './components/header';
import { FooterModule } from './components/footer';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        HeaderModule,
        FooterModule
    ],
    declarations: [
        LayoutComponent,
        LayoutBaseComponent
    ],
    exports: [
        LayoutComponent,
        LayoutBaseComponent
    ],
})
export class LayoutModule {}
