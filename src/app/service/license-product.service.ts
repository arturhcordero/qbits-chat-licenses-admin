import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LicenseProduct } from '../model/product/license-product';

import { environment } from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class LicensesProductService {

    constructor(protected http: HttpClient) { }

    findLicenseProducts(): Observable<LicenseProduct[]> {

        const endpoint = `${environment.chatLicensesUrl}/licenseProduct?licenseProductTypeId=1`;

        return this.http.get<LicenseProduct[]>(endpoint);
   }

}
