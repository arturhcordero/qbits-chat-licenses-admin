import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Feature } from '../model/product/feature';

import { environment } from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class FeaturesService {

    constructor(protected http: HttpClient) { }

    getFeatures(): Observable<Feature[]> {
        const endpoint = `${environment.chatLicensesUrl}/catalog/feature`;
        return this.http.get<Feature[]>(endpoint);
    }
}
