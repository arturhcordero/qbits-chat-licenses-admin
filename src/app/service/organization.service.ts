import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Organization } from '../model/organization/organization';
import { SimpleMessage } from '../model/util/simple-message';

import { environment } from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class OrganizationService {

    constructor(protected http: HttpClient) {

    }

    findOrganizationInfo(organizationId: number): Observable<Organization> {
        const url = `${environment.chatLicensesUrl}/organization/${organizationId}/info`;
        return this.http.get<Organization>(url);
    }

    modifyOrganizationInfo(organizationInfo: Organization): Observable<SimpleMessage> {
        const url = `${environment.chatLicensesUrl}/organization/modify`;
        return this.http.patch<SimpleMessage>(url, organizationInfo);
    }

}
