import { License } from '../model/account/license/license';
import { PaymentMethod } from '../model/payment-method/payment-method';
import { Organization } from '../model/organization/organization';
import { OrganizationType } from '../model/organization/organization-type';
import { ProductOrder } from '../model/order/product-order';
import { LicenseProduct } from '../model/product/license-product';
import { Feature } from '../model/product/feature';
import {UserInfo} from '../model/account/user-info';
import {Page} from '../model/util/page';
import {UserLicenseInfo} from '../model/account/license/user-license-info';
import {Order} from '../model/product/request/order';
import {RoleInfo} from '../model/account/role/role-info';

export class SessionInfo {

    organizationId: number;
    roles: RoleInfo[];
    orderLicense: Order;
    usersLicensesPage: Page<UserLicenseInfo>;
    licenses: License[];
    authenticatedUser: UserInfo;
    authorities: string[];
    productOrders: ProductOrder[];
    currentLicenseProduct: LicenseProduct;
    features: Feature[];
    licenseProducts: LicenseProduct[];
    organizationTypes: OrganizationType[];
    license: License;
    organization: Organization;
    paymentMethods: PaymentMethod[];
    defaultPaymentMethod: PaymentMethod;
    currentPaymentMethod: PaymentMethod;

}
