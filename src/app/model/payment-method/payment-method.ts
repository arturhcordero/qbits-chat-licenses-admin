
export class PaymentMethod {

    uuid: string = null;
    organizationId: number = null;
    alias: string = null;
    customerName: string = null;
    lastDigits: string = null;
    expirationDate: string = null;
    bank: string = null;
    brand: string = null;
    type: number = null;
    defaultPayment: boolean = false;
    icon: string = null;

}
