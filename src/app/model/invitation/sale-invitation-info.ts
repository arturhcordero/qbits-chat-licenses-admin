import {ProductOrder} from '../order/product-order';

export class SaleInvitationInfo {

    id: number;
    email: string;
    status: SaleInvitationStatus;
    licenseProduct: LicenseProduct;
    productOrder: ProductOrder;

}

class SaleInvitationStatus {
    identifier: number;
    name: string;
    description: string;
    icon: string;
}

class LicenseProduct {
    name: string;
    description: string;
    iconUrl: string;
}
