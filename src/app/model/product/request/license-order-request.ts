import {Order} from './order';

export class LicenseOrderRequest {

    order: Order;
    organizationId: number;

}
