import { OrderDetail } from './order-detail';

export class Order {

    total: number = 0;
    orderDetails: OrderDetail[] = [];

    calculateTotal() {
        this.total = 0.0;
        this.orderDetails.forEach(od => this.total += od.subtotal);
    }

}
