
export class Page<T> {
    content: T[];
    totalElements: number;
    pageable: Pageable;
}

export class Pageable {
    pageNumber: number;
}
