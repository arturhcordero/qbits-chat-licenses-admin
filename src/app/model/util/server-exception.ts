
export class ServerException {

    timestamp: Date;
    status: number;
    error: string;
    message: string;
    messageCode: number;
    path: string;

}
