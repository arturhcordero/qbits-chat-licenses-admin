import {Moment} from 'moment';

export class DateData {

    months: Month[] = [];
    years: number[] = [];

    addMonth(month: Month) {
        this.months.push(month);
    }

    addYear(year: number) {
        this.years.push(year);
    }

    findMonth(identifier: string): Month {
        return this.months.filter(month => month.identifier == identifier)[0];
    }

}

export class Month {

    identifier: string;
    name: string;
    value: Moment;
    dayList: number[] = [];

    constructor(identifier: string, name: string, value: Moment) {
        this.identifier = identifier;
        this.name = name;
        this.value = value;

        for (let i = 1; i <= this.value.daysInMonth(); i++)
            this.dayList.push(i);

    }

}
