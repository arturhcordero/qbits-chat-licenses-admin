
import * as moment from 'moment';
import 'moment/locale/es';
import {Cellphone} from './cellphone';
import {Settings} from '../../../config/settings';

export class AccountInfo {

    fullName: string;
    email: string;
    password: string;
    repeatPassword: string;
    birthdaySt: string;
    cellphone: Cellphone = new Cellphone();
    birthday: Birthday = new Birthday();
    genderReferenceId: number;

    constructor() {
        moment.locale(Settings.defaultLocale);
    }

}

export class Birthday {

    emptyValue: string = Settings.emptyValue;

    day: string = this.emptyValue;
    month: string = this.emptyValue;
    year: string = this.emptyValue;
    fullAge: boolean = false;

    isValid(withFullAge: boolean = true) {

        let nullValidation: boolean = this.day != this.emptyValue &&
                this.month != this.emptyValue &&
                this.year != this.emptyValue;

        if (withFullAge)
            return nullValidation && this.fullAge;

        return nullValidation;
    }

    setValues(day: string, month: string, year: string) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    export() {
        let monthSt = `0${(parseInt(this.month) + 1).toString()}`.slice(-2);
        let daySt = `0${(parseInt(this.day)).toString()}`.slice(-2);
        return `${daySt}/${monthSt}/${this.year}`;
    }

}
