import { LicenseProductInfo } from '../../product/license-product-info';
import { UserInfo } from './user-info';
import { DurationType } from '../duration-type';

export class License {

    id: number = null;
    quantity: number = null;
    organizationId: number = null;
    expiration: string = null;
    describeExpiration: string = null;
    status: LicenseStatus = null;
    duration: DurationType;
    licenseProductInfo: LicenseProductInfo = new LicenseProductInfo();
    userInfo: UserInfo = new UserInfo();

}

class LicenseStatus {

    identifier: number;
    description: string;

}
