
export class LicenseAssignmentRequest {

    email: string;
    message: string;
    licenseId: number;
    roleId: number;

    constructor() {
        this.message = `Bienvenido a Q-Bits Chat. Confirma tu cuenta de acceso e inicia sesión para que comencemos una conversación.`;
    }


}
