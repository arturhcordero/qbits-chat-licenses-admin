import { UserInfo } from '../user-info';
import { License } from './license';

export class UserLicenseInfo {

    user: UserInfo;
    license: License;

}
