import {LicenseProductInfo} from '../product/license-product-info';

export class OrderDetail {

    id: number;
    createdAt: string;
    quantity: number;
    price: number;
    tax: number;
    subtotal: number;
    licenseProduct: LicenseProductInfo;

    periodStartDate: string;
    periodEndDate: string;

}
