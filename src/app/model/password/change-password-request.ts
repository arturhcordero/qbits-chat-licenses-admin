
export class ChangePasswordRequest {

    email: string;
    currentPassword: string;
    newPassword: string;
    repeatNewPassword: string;

}
