
export class SendQrCodeRequest {

    email: string;
    configuration: number;

    constructor(email: string) {
        this.email = email;
        this.configuration = 1;
    }

}
