import { OrganizationType } from './organization-type';
import { Address } from './address';

export class Organization {

    id: number;
    email: string;
    name: string;
    description: string;
    rfc: string;
    organizationType: OrganizationType;
    addresses: Address[] = [];

}
