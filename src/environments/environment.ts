// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,

    chatLicensesUrl: 'http://172.16.38.208:8095',
    // chatLicensesUrl: 'http://qbits-chat-licenses-dev.us-east-1.elasticbeanstalk.com',
    authServerUrl: 'http://qbits-chat-licenses-auth-dev.us-east-1.elasticbeanstalk.com/oauth/token',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
